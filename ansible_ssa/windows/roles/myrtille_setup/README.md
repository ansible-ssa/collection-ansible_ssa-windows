# Ansible Role: myrtille_setup

This role will help to deploy the Myrtille Tool (https://www.myrtille.io/).

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
```

If you want to use this role in your playbook:

```yaml

---
- name: Deploy Windows Myrtille
   ansible.builtin.include_role:
     name: ansible_ssa.windows.myrtille_setup
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
