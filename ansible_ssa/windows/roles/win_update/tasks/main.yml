---
- block:
    - name: Check for missing Windows updates
      ansible.windows.win_updates:
        category_names: "{{ win_update_category_names }}"
        state: searched
      register: available_updates

    - ansible.builtin.debug:
        msg: |
          {{ inventory_hostname }} has {{ available_updates.found_update_count }} updates available.
          {% for update in updates %}
            - {{ update.title }}
          {% endfor %}
      vars:
        updates: "{{ (available_updates.updates.values() | list) if (available_updates.updates is mapping) else (available_updates.updates) }}"
      when: available_updates.updates is defined

    - ansible.builtin.debug:
        var: available_updates

    - name: Install all Windows updates
      ansible.windows.win_updates:
        category_names: "{{ win_update_category_names }}"
        reject_list: "{{ win_update_blacklist | default(omit) }}"
        accept_list: "{{ win_update_whitelist | default(omit) }}"
        reboot: yes
      register: installed_updates
      when: available_updates.found_update_count|int >= 1

  rescue:
    - name: Reboot before continue to retry
      ansible.windows.win_reboot:

    - name: Install all Windows updates (retry)
      ansible.windows.win_updates:
        category_names: "{{ win_update_category_names }}"
        reject_list: "{{ win_update_blacklist | default(omit) }}"
        accept_list: "{{ win_update_whitelist | default(omit) }}"
        reboot: yes
      register: installed_updates_retry

  always:
    - name: Check for missing Windows updates
      ansible.windows.win_updates:
        state: searched
      register: available_updates

    - name: List missing Windows updates
      ansible.builtin.debug:
        var: available_updates

    - name: Report on installed Windows updates
      ansible.builtin.debug:
        msg: |
          Host {{ inventory_hostname }} has been patched and rebooted. {{ installed_updates.installed_update_count | default('0') }} updates were installed. The updates installed were:

          {% for update in updates %}
            - {{ update.title }}
          {% endfor %}
          {{ installed_updates.failed_update_count | default('0') }} updates failed to install."
      vars:
        updates: "{{ (installed_updates.updates.values() | list) if (installed_updates.updates is mapping) else (installed_updates.updates) }}"
      when: installed_updates.updates is defined
